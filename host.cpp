#include <e-hal.h>
#include <e-loader.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <inttypes.h>
#include <string>
#include <iostream>

#include <boost/config.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/cmdline.hpp>

#include <boost/asio.hpp>

#include "libs/common.h"
#include "libs/c_png.h"


#define ROWS    4
#define COLS    4
#define CORECOUNT  (COLS * ROWS)
#define BUF_OFFSET  0x01000000
#define true  1
#define false  0

#define ERR_CMD_LINE    1
#define ERR_IMG_FORMAT  2

typedef struct {
    e_platform_t eplat;
    e_epiphany_t edev;
    e_mem_t emem;
} ep_context_t;

using namespace std;

double elapsed_seconds(struct timeval start, struct timeval end) {
    return end.tv_sec + (end.tv_usec / 1000000.0) - start.tv_sec - (start.tv_usec / 1000000.0);
}

int init_epiphany(ep_context_t *e, msg_block_t *msg, size_t buf_size, int direct) {
    e_init(NULL);
    e_reset_system();
    e_get_platform_info(&e->eplat);
    // buf_size: sizeof(msg_block_t) + depth-buffer-size
    // emem memory map
    // emem start----
    // msg_block_t msg
    // uint8_t image_buffer[IMAGE_WIDTH * IMAGE_HEIGHT * IMAGE_BYTES]
    // emem end------
    e_alloc(&e->emem, BUF_OFFSET, buf_size);
    e_write(&e->emem, 0, 0, 0, (void *) msg, sizeof (msg_block_t));
    e_open(&e->edev, 0, 0, ROWS, COLS);
    if (e_load_group((!direct ? (char *) "epiphany.srec" : (char *) "epiphany_direct.srec"), &e->edev, 0, 0, ROWS, COLS, E_TRUE) == E_ERR) {
        cerr << "e_load failed\n\r";
        return -1;
    }

    int row, col;
    for (row = 0; row < ROWS; row++) {
        for (col = 0; col < COLS; col++) {
            e_resume(&e->edev, row, col);
        }
    }

    return 0;
}

void release_epiphany(ep_context_t *e) {
    e_close(&e->edev);
    e_free(&e->emem);
    e_finalize();
}

static inline void nano_wait(uint32_t sec, uint32_t nsec) {
    struct timespec ts;
    ts.tv_sec = sec;
    ts.tv_nsec = nsec;
    nanosleep(&ts, NULL);
}

namespace po = boost::program_options;
using boost::asio::ip::tcp;

int main(int argc, char *argv[]) {
    // Create epiphany device descriptive context

    // Declare the supported options.
    po::options_description desc("Program arguments");

    static ep_context_t ep_context;
    bool debug = false, verbose = false, csv = false, csv_header = false,
            csv_dump = false;
    int iterations;
    string input_file;

    desc.add_options()
            ("help,h", "produce help message")
            ("debug,d", po::value<bool>(&debug)->implicit_value(true), "Enter debug mode")
            ("csv,c", po::value<bool>(&csv)->implicit_value(true), "Print statistics in csv")
            ("csv-header,H", po::value<bool>(&csv_header)->implicit_value(true), "Print csv header")
            ("csv-dump,D", po::value<bool>(&csv_dump)->implicit_value(true), "Dump all test results in csv")
            ("verbose,v", po::value<bool>(&verbose)->implicit_value(true), "Print more information")
            ("iterations,i", po::value<int>(&iterations)->default_value(20), "Number of iterations to test the core")
            ("input-file,I", po::value<string>(&input_file)->required(), "Input image file (Grayscale PNG)");
    if (debug) {
        cout << "Debug mode active\n\r";
    }
    
    po::positional_options_description p;
    p.add("input-file", 1);

    po::variables_map vm;
    try {
        po::store(po::command_line_parser(argc, argv).
                options(desc).positional(p).run(), vm);
        po::notify(vm);
    } catch (boost::program_options::required_option& e) {
        cerr << "ERROR: " << e.what() << std::endl << std::endl;
        return ERR_CMD_LINE;
    } catch (boost::program_options::error& e) {
        cerr << "ERROR: " << e.what() << std::endl << std::endl;
        return ERR_CMD_LINE;
    }
    
    
    if(debug) {
        cout << "Input image file: " << input_file << "\n\r";
    }
    
    // Create an image buffer
    uint8_t* image_buffer;
    int width, height;

    // Read image from png file
    image_buffer = read_pngpp_file((char*) input_file.c_str(), &width, &height);
    
    if(debug) {
        cout << "Image dimensions " << width << "x" << height << "\n\r";
    }
    
    // Max pixel fit per core is currently 12,2k
    // This number may be slightly increased, but it is empirical
    bool direct_write = (((height * width) / 16) <= 12200);

    if(direct_write && debug) {
        cout << "Info: using direct epiphany memory write..\n\r";
    }
    
    // Create message structure for host-device memory share
    msg_block_t msg;
    memset(&msg, 0, sizeof (msg_block_t));
    uint8_t I_du[width * height], I_dv[width * height];
    memset(I_du, 0, width * height);
    memset(I_dv, 0, width * height);

    // All shared memory buffer size
    size_t buf_size = (3 * width * height) + sizeof (msg_block_t);

    // Other variables used in processing
    uint32_t i, row, col, vepiphany[CORECOUNT], vhost[CORECOUNT];
    for (i = 0; i < CORECOUNT; i++) {
        vepiphany[i] = 0;
        vhost[i] = 0;
    }

    struct timeval time_total_start, time_total_end, time_write_start, 
            time_write_end, time_exec_start, time_exec_end, time_read_start,
            time_read_end;
    double elapsed_total[iterations], elapsed_write[iterations], 
            elapsed_exec[iterations], elapsed_read[iterations];

    // Set data for epiphany device
    // Output image memory address offset
    msg.msg_init.smem_start = sizeof (msg_block_t) + (size_t) width * height;
    // Input image memory address offset
    msg.msg_init.start_offset = sizeof (msg_block_t);
    // input image width / height
    msg.msg_init.width = width;
    msg.msg_init.height = height;
    // Grayscale image contains 1 byte per pixel
    msg.msg_init.pixel_bytes = 1;
    
    // Initiate the coprocessor
    if (init_epiphany(&ep_context, &msg, buf_size, direct_write) < 0) {
        perror("Error: init_epiphany");
        abort();
    } else if (debug) {
        cout << "Initiated epiphany succesfully!\n\r";
    }
    
    nano_wait(1, 0);
    boost::asio::io_service io_service;
    tcp::resolver resolver(io_service);
    tcp::resolver::query query(tcp::v4(), "192.168.1.91", "8000");
    tcp::resolver::iterator iterator = resolver.resolve(query);
    tcp::socket s(io_service);
    boost::asio::connect(s, iterator);
    char reply[5];
    size_t reply_length;
    
    for (i = 0; i < iterations; i++) {
        boost::asio::write(s, boost::asio::buffer("-- start --", 11));
        reply_length = boost::asio::read(s,
                boost::asio::buffer(reply, 2));
        // Measure current time
        gettimeofday(&time_total_start, NULL);
        gettimeofday(&time_write_start, NULL);
        
        if (direct_write) {
            for (row = 0; row < ROWS; row++) {
                for (col = 0; col < COLS; col++) {
                    // Copy a portion of data straight into eCores memory
                    e_write(&ep_context.edev, row, col, BUF_IN_ADDRESS, (void *) (image_buffer + ((row * COLS + col) * 12200)), (size_t) 12200);
                    // Let the core start counting while copying the others
                    e_resume(&ep_context.edev, row, col);
                }
            }
        } else {
            // Write the image to shared DRAM block for DMA reading
            e_write(&ep_context.emem, 0, 0, (off_t) sizeof (msg_block_t), (void *) image_buffer, (size_t) width * height);
            
            // wake-up eCore
            for (row = 0; row < ROWS; row++) {
                for (col = 0; col < COLS; col++) {
                    e_resume(&ep_context.edev, row, col);
                }
            }
            
        }
        gettimeofday(&time_write_end, NULL);
        
        gettimeofday(&time_exec_start, NULL);
        // Cycle for each core (Why so complicated?)
        for (row = 0; row < ROWS; row++) {
            for (col = 0; col < COLS; col++) {
                int core = row * COLS + col;
                if (verbose) {
                    printf("Waiting for core #%d to complete\n\r", core);
                }
                while (1) {
                    // wait for completion (eCore job)
                    // Read core frame counter
                    e_read(
                            &ep_context.emem, 0, 0, (off_t) ((char *) &msg.msg_d2h[core] - (char *) &msg), (void *) &msg.msg_d2h[core], sizeof (msg_dev2host_t)
                            );

                    vepiphany[core] = msg.msg_d2h[core].value[0];
                    // Check whether core counter increased
                    if (vhost[core] - vepiphany[core] > ((~(uint32_t) 0) >> 1)) {
                        break;
                    }
                    nano_wait(0, 10000);
                }
                vhost[core] = vepiphany[core];
            }
        }
        
        gettimeofday(&time_exec_end, NULL);
        
        gettimeofday(&time_read_start, NULL);
        e_read(
                &ep_context.emem,
                0,
                0,
                (off_t) sizeof (msg_block_t) + (size_t) (width * height),
                (void *) I_du,
                (size_t) width * height
                );
        e_read(
                &ep_context.emem,
                0,
                0,
                (off_t) sizeof (msg_block_t) + (size_t) (width * height * 2),
                (void *) I_dv,
                (size_t) width * height
                );
        gettimeofday(&time_read_end, NULL);
        gettimeofday(&time_total_end, NULL);
        
        boost::asio::write(s, boost::asio::buffer("-- stop --", 10));
        reply_length = boost::asio::read(s,
                boost::asio::buffer(reply, 2));
        
        elapsed_total[i] = elapsed_seconds(time_total_start, time_total_end);
        elapsed_write[i] = elapsed_seconds(time_write_start, time_write_end);
        elapsed_exec[i] = elapsed_seconds(time_exec_start, time_exec_end);
        elapsed_read[i] = elapsed_seconds(time_read_start, time_read_end);
    }
    
    release_epiphany(&ep_context);
    boost::asio::write(s, boost::asio::buffer("** quit **", 10));
    if(debug || csv) {
        double total = 0, write = 0, exec = 0, read = 0,
                totalVar = 0;
        for(int i = 0; i < iterations; i++) {
            total += elapsed_total[i];
            write += elapsed_write[i];
            exec += elapsed_exec[i];
            read += elapsed_read[i];
        }
        total /= iterations;
        write /= iterations;
        exec /= iterations;
        read /= iterations;
        for(int i = 0; i < iterations; i++) {
            totalVar += pow((elapsed_total[i] - total), 2);
        }
        totalVar /= iterations;
        double totalDeviation = sqrt(totalVar);
        if(debug) {
            cout << "Processing complete. Average statistics: \n\r";
            cout << "Write:\t" << write * 1000 << " ms\n\r";
            cout << "Exec:\t" << exec * 1000 << " ms\n\r";
            cout << "Read:\t" << read * 1000 << " ms\n\r";
            cout << "Total:\t" << total * 1000 << " ms\n\r";
            cout << "Variation:\t" << totalVar << "\n\r";
            cout << "Deviation:\t" << totalDeviation << "\n\r";
            cout << "FPS:\t" << 1 / total << "\n\r";
        }

        if(csv) {
            if(csv_header) {
                cout << "writeAvg,execAvg,readAvg,totalAvg,totalVar,totalDev,fpsAvg" << "\n\r" ;
            }
            cout << write << "," << exec << "," << read << "," << total;
            cout << "," << totalVar << "," << totalDeviation;
            cout << "," << 1 / total << "\n\r";
        }
        if(csv_dump) {
            if(csv_header) {
                cout << "num,write,exec,read,total" << "\n\r" ;
            }
            for(int i = 0; i < iterations; i++) {
                cout << i << "," << elapsed_write[i] << "," << elapsed_exec[i];
                cout << "," << elapsed_read[i] << "," << elapsed_total[i], "\n\r";
            }
                    
        }
    }

    // Write output files
    write_pngpp_file((char *) "output_du_esdk.png", I_du, width, height);
    write_pngpp_file((char *) "output_dv_esdk.png", I_dv, width, height);

    return 0;
}

