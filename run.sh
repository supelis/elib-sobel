ESDK=${EPIPHANY_HOME}
ELIBS=${ESDK}/tools/host/lib:${LD_LIBRARY_PATH}
EHDF=${EPIPHANY_HDF}
ITER=100

echo -n "Executing 480p image"
sudo -E LD_LIBRARY_PATH=${ELIBS} EPIPHANY_HDF=${EHDF} ./elib_sobel --csv on --csv-header on images/480p-example.png -i ${ITER} > results/out.csv
echo "."
echo -n "Executing 720p image"
sudo -E LD_LIBRARY_PATH=${ELIBS} EPIPHANY_HDF=${EHDF} ./elib_sobel --csv on images/720p-example.png -i ${ITER} >> results/out.csv
echo "."
echo -n "Executing 1080p image"
sudo -E LD_LIBRARY_PATH=${ELIBS} EPIPHANY_HDF=${EHDF} ./elib_sobel --csv on images/1080p-example.png -i ${ITER} >> results/out.csv
echo "."
echo "Results:"
cat results/out.csv
