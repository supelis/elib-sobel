#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=e-gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/host.o \
	${OBJECTDIR}/libs/c_png.o \
	${OBJECTDIR}/libs/image.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-O3
CXXFLAGS=-O3

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/lib/arm-linux-gnueabihf/ -L/opt/adapteva/esdk/tools/host.armv7l/lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/elib_sobel

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/elib_sobel: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/elib_sobel ${OBJECTFILES} ${LDLIBSOPTIONS} -lpng -le-hal -le-loader -lboost_program_options -lboost_system -lboost_thread -lpthread

${OBJECTDIR}/host.o: host.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/opt/adapteva/esdk/tools/host/include -std=c++11 -lpthread -lm -lrt -lpng -le-hal -le-loader -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/host.o host.cpp

${OBJECTDIR}/libs/c_png.o: libs/c_png.cpp 
	${MKDIR} -p ${OBJECTDIR}/libs
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/opt/adapteva/esdk/tools/host/include -std=c++11 -lpng -L/usr/include/libpng -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/libs/c_png.o libs/c_png.cpp

${OBJECTDIR}/libs/image.o: libs/image.cpp 
	${MKDIR} -p ${OBJECTDIR}/libs
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/opt/adapteva/esdk/tools/host/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/libs/image.o libs/image.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/elib_sobel

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
