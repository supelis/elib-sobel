## -*- Makefile -*-
##
## User: rokas
## Time: Sep 17, 2015 10:07:14 PM
## Makefile created by Oracle Solaris Studio.
##
## This file is generated automatically.
##
ESDK=/opt/adapteva/esdk
ELIBS=$(ESDK)/tools/host/lib
EINCS=$(ESDK)/tools/host/include
ELDF=$(ESDK)/bsps/current/internal.ldf
RESOURCES=epiphany.srec epiphany_direct.srec #epiphany_5x5.srec 
E_OBJS=epiphany.elf epiphany_direct.elf #epiphany_5x5.elf
PATH+=:/opt/adapteva/esdk/tools/e-gnu/bin
CFLAGS_E=-O3 -ffast-math -funroll-loops -Wall
LIBS_E=-le-lib -lm


all: resources

## Target: resources
resources: $(RESOURCES) clean-e-objs

clean-e-objs:
	    rm $(E_OBJS)

clean-resources:
	    rm $(RESOURCES)

e-objs: $(E_OBJS)
	
epiphany.elf: epiphany.c common.h
	e-gcc epiphany.c $(CFLAGS_E) -T$(ELDF) $(LIBS_E) -o epiphany.elf

epiphany_5x5.elf: epiphany_5x5.c common.h
	e-gcc epiphany_5x5.c $(CFLAGS_E) -T$(ELDF) $(LIBS_E) -o epiphany_5x5.elf

epiphany_direct.elf: epiphany_5x5.c common.h
	e-gcc epiphany_direct.c $(CFLAGS_E) -T$(ELDF) $(LIBS_E) -o epiphany_direct.elf

epiphany.srec: epiphany.elf
	e-objcopy --srec-forceS3 --output-target srec epiphany.elf epiphany.srec

epiphany_5x5.srec: epiphany_5x5.elf
	e-objcopy --srec-forceS3 --output-target srec epiphany_5x5.elf epiphany_5x5.srec

epiphany_direct.srec: epiphany_direct.elf
	e-objcopy --srec-forceS3 --output-target srec epiphany_direct.elf epiphany_direct.srec


#### Clean target deletes all generated files ####
clean:

# Enable dependency checking
.KEEP_STATE:
.KEEP_STATE_FILE:.make.state.GNU-armv7-Linux

