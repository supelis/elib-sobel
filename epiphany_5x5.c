/*
 * epiphany.c
 *
 *  Created on: Apr 24, 2015
 *      Author: rokas
 */

#include "e_lib.h"
#include "common.h"

#include <math.h>

#define BUF_ADDRESS 0x8f000000
#define BUF_IN_ADDRESS 0x5000
#define BUF_DU_OUT_ADDRESS 0x2000
#define BUF_DV_OUT_ADDRESS 0x3500

unsigned char saturate (int in) {
	return (in < 0) ? 0 : (in > 255) ? 255 : (unsigned char) in;
}

int main(void) {
	msg_block_t msg;
	msg_block_t *shared_msg = (msg_block_t *) (BUF_ADDRESS);

	e_coreid_t coreid = e_get_coreid();
	unsigned int row, col, core, cores, x, y, j;

	// Get core ID
	e_coords_from_coreid(coreid, &row, &col);

	// Calculate core count and core number
	core = row * e_group_config.group_cols + col;
	cores = e_group_config.group_rows * e_group_config.group_cols;
	unsigned int frame = 1,
				 next_line = msg.msg_init.width;

	// to fix timing bug
	e_wait(E_CTIMER_0, 0x10000000);

	// get init data
	e_dma_copy(&msg.msg_init, &shared_msg->msg_init, sizeof(msg_init_t));

	// Initiate input image start address
	uint8_t *image_start_address = (uint8_t *)BUF_ADDRESS + msg.msg_init.start_offset;
	uint8_t *image_du_start = image_start_address + (msg.msg_init.width * msg.msg_init.height);
	uint8_t *image_dv_start = image_start_address + (msg.msg_init.width * msg.msg_init.height * 2);

	// Temporary value store for calculations
	int16_t temp_du, temp_dv;

	// Cast a output buffer for a single iteration
	size_t fb_length = (size_t) ((msg.msg_init.width) - 4);

	// Calculate lines per core
	int lines_per_core = msg.msg_init.height / cores;

	// Calculate image pixel start offset
	int start_offset = lines_per_core * core,
			end_offset;

	// Calculate end pixel for the core.
	// Set the last core value to hardcode to avoid segmentation fault
	if(core == (cores - 1)) {
		end_offset = msg.msg_init.height;
	} else {
		end_offset = (lines_per_core * (core + 1)) - 1;
	}

	while (1) {
		// Wait for request to begin calculations
		__asm__ __volatile__ ("trap 4");

		// Set informational values for the host program to see
		msg.msg_d2h[core].coreid = coreid;
		msg.msg_d2h[core].value[0] = frame;

		// Output DMA buffer address for currently calculating row
		uint8_t *fb_du_address = image_du_start + (start_offset * msg.msg_init.width) + 2 * msg.msg_init.width + 2;
		uint8_t *fb_dv_address = image_dv_start + (start_offset * msg.msg_init.width) + 2 * msg.msg_init.width + 2;

		// Input DMA address for input image rows required for calculations
		uint8_t *src_address = image_start_address + (start_offset * msg.msg_init.width);

		uint8_t *I = (uint8_t *) (BUF_IN_ADDRESS);

		// Start calculations each core selects rows to calculate
		for (y = start_offset; y <= end_offset; y++) {
			// TODO: improve input image line buffer to copy three rows of pixel on first
			// iteration and then pushing only one additional line of pixels to reduce
			// memory traffic

			if(y == start_offset) {
				// Copy four lines of the input image
				e_dma_copy(I, src_address, msg.msg_init.width * 5);
				src_address += next_line * 4;
			} else {
				// Move last two lines up
				uint8_t *pix_start = I, *pix_mid = I + msg.msg_init.width;
				for (j = 0; j < 4 * msg.msg_init.width; j++, pix_start++, pix_mid++) {
					*pix_start = *pix_mid;
				}
				// Copy a two lines this time only
				e_dma_copy((I + 4 * msg.msg_init.width), src_address, next_line);
			}

			// init pixel pointers
			uint8_t *p00 = I,
					*p01 = I + 1,
					*p02 = I + 2,
					*p03 = I + 3,
					*p04 = I + 4,
					*p10 = I + msg.msg_init.width,
					*p11 = I + 1 + msg.msg_init.width,
					*p12 = I + 2 + msg.msg_init.width,
					*p13 = I + 3 + msg.msg_init.width,
					*p14 = I + 4 + msg.msg_init.width,
					*p20 = I + 2 * msg.msg_init.width,
					*p21 = I + 1 + 2 * msg.msg_init.width,
					*p22 = I + 2 + 2 * msg.msg_init.width,
					*p23 = I + 3 + 2 * msg.msg_init.width,
					*p24 = I + 4 + 2 * msg.msg_init.width,
					*p30 = I + 3 * msg.msg_init.width,
					*p31 = I + 1 + 3 * msg.msg_init.width,
					*p32 = I + 2 + 3 * msg.msg_init.width,
					*p33 = I + 3 + 3 * msg.msg_init.width,
					*p34 = I + 4 + 3 * msg.msg_init.width,
					*p40 = I + 4 * msg.msg_init.width,
					*p41 = I + 1 + 4 * msg.msg_init.width,
					*p42 = I + 2 + 4 * msg.msg_init.width,
					*p43 = I + 3 + 4 * msg.msg_init.width,
					*p44 = I + 4 + 4 * msg.msg_init.width,
					*pix_du = (uint8_t *) BUF_DU_OUT_ADDRESS,
					*pix_dv = (uint8_t *) BUF_DV_OUT_ADDRESS,
					*end_pixel = I + (msg.msg_init.width * 5) - 1;

			// Calculate the row
			for (x = 0; p44 != end_pixel; p00++, p01++, p02++, p03++, p04++,
									 	  p10++, p11++, p12++, p13++, p14++,
										  p20++, p21++, p22++, p23++, p24++,
										  p30++, p31++, p32++, p33++, p34++,
										  p40++, p41++, p42++, p43++, p44++,
										  pix_du++, pix_dv++, x++) {
				/** Sx frame
				 *  ---                  ---
				 *  |  1   2   0   -2  -1  |
				 *  |  4   8   0   -8  -4  |
				 *  |  6  12   0  -12  -6  |
				 *  |  4   8   0   -8  -4  |
				 *  |  1   2   0   -2  -1  |
				 *  ---                  ---
				 */
				temp_du =   	   *p00 +  2 * *p01 -  2 * *p03 -     *p04
							 + 4 * *p10 +  8 * *p11 -  8 * *p13 - 4 * *p14
							 + 6 * *p20 + 12 * *p21 - 12 * *p23 - 6 * *p24
							 + 4 * *p30 +  8 * *p31 -  8 * *p33 - 4 * *p34
							 +     *p40 +  2 * *p41 -  2 * *p43 -     *p44;
				// scale 1/32 then clamp to [-128; 128] and saturate [0; 255]
				*pix_du = saturate(temp_du * 0.03125f + 128);


				/** Sy frame
				 *  ---                 ---
				 *  | -1  -4  -6  -4  -1  |
				 *  | -2  -8 -12  -8  -2  |
				 *  |  0   0   0   0   0  |
				 *  |  2   8  12   8   2  |
				 *  |  1   4   6   4   1  |
				 *  ---                 ---
				 */
				temp_dv =    -     *p00 - 4 * *p01 -  6 * *p02 - 4 * *p03 -     *p04
						     - 2 * *p10 - 8 * *p11 - 12 * *p12 - 8 * *p13 - 2 * *p14
						     + 2 * *p30 + 8 * *p31 + 12 * *p32 + 8 * *p33 + 2 * *p34
						     +     *p40 + 4 * *p41 +  6 * *p42 + 8 * *p43 +     *p44;
				// scale 1/32 then clamp to [-128; 128] and saturate [0; 255]
				*pix_dv = saturate(temp_dv * 0.03125f + 128);
			}

			// copy to the output line to DMA resource
			e_dma_copy(fb_du_address, (uint8_t *) (BUF_DU_OUT_ADDRESS), (size_t) x);
			e_dma_copy(fb_dv_address, (uint8_t *) (BUF_DV_OUT_ADDRESS), (size_t) x);

			// Move to next lines to calculate
			fb_du_address += next_line;
			fb_dv_address += next_line;
			src_address += next_line;

		}

		// notify completion
		e_dma_copy(&shared_msg->msg_d2h[core], &msg.msg_d2h[core], sizeof(msg_dev2host_t));

		// Increase frame counter
		frame++;
	}

	return 0;

}

