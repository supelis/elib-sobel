/*
 * epiphany.c
 *
 *  Created on: Apr 24, 2015
 *      Author: rokas
 */


#include "e_lib.h"
#include "libs/common.h"

unsigned char saturate (int in) {
	return (in < 0) ? 0 : (in > 255) ? 255 : (unsigned char) in;
}

int main(void) {
	msg_block_t msg;
	msg_block_t *shared_msg = (msg_block_t *) (BUF_ADDRESS);
	uint8_t *I = (uint8_t *) (BUF_IN_ADDRESS);

	e_coreid_t coreid = e_get_coreid();
	unsigned int row, col, core, cores;

	e_coords_from_coreid(coreid, &row, &col);

	// Get core ids
	core = row * e_group_config.group_cols + col;
	cores = e_group_config.group_rows * e_group_config.group_cols;
	unsigned int frame = 1;//, next_line = (msg.msg_init.width * 2);

	// to fix timing bug
	e_wait(E_CTIMER_0, 0x10000000);

	// get init data
	e_dma_copy(&msg.msg_init, &shared_msg->msg_init, sizeof(msg_init_t));

	// Initiate input image start address
	uint8_t *image_start_address = (uint8_t *)BUF_ADDRESS + msg.msg_init.start_offset;
	uint8_t *image_du_start = image_start_address + (msg.msg_init.width * msg.msg_init.height);
	uint8_t *image_dv_start = image_start_address + (msg.msg_init.width * msg.msg_init.height * 2);

	// Temporary value store for calculations
	int16_t temp_du, temp_dv;

	// Cast a output buffer size
	//size_t fb_length = (2 * msg.msg_init.width) - 2;

	int lines_per_core = msg.msg_init.height / cores;

	int start_offset = lines_per_core * core;/*, end_offset;

	if(core == (cores - 1)) {
		end_offset = msg.msg_init.height;
	} else {
		end_offset = (lines_per_core * (core + 1)) - 1;
	}*/

	// Output DMA buffer address for currently calculating row
	uint8_t *fb_du_address = image_du_start + (start_offset * msg.msg_init.width) + msg.msg_init.width + 1;
	uint8_t *fb_dv_address = image_dv_start + (start_offset * msg.msg_init.width) + msg.msg_init.width + 1;

	while (1) {
		// Wait for request to begin calculations
		__asm__ __volatile__ ("trap 4");

		// Set informational values for the host program to see
		msg.msg_d2h[core].coreid = coreid;
		msg.msg_d2h[core].value[0] = frame;



		// Input DMA address for input image rows required for calculations
		//uint8_t *src_address = image_start_address + (start_offset * msg.msg_init.width);

		// Start calculations each core selects rows to calculate
		//for (y = start_offset; y < end_offset; y += 2) {
			// TODO: improve input image line buffer to copy three rows of pixel on first
			// iteration and then pushing only one additional line of pixels to reduce
			// memory traffic

			// Currently calculated pixel address in a 3x3 frame

			/*if (y == start_offset) {
				e_dma_copy(I, src_address, msg.msg_init.width * 4);
				src_address += next_line;
			} else {
				// Move last two lines up
				uint8_t *pix_start = I, *pix_mid = I + next_line;
				for (j = 0; j < next_line; j++, pix_start++, pix_mid++) {
					*pix_start = *pix_mid;
				}
				// Copy a two lines this time only
				e_dma_copy((I + next_line), src_address, next_line);
			}*/

			// init pixel pointers
			uint8_t *pix_du = (uint8_t *) (BUF_DU_OUT_ADDRESS),
					*pix_dv = (uint8_t *) (BUF_DV_OUT_ADDRESS),
					*p00 = I,
					*p01 = I + 1,
					*p02 = I + 2,
					*p10 = I + msg.msg_init.width,
					*p11 = I + 1 + msg.msg_init.width,
					*p12 = I + 2 + msg.msg_init.width,
					*p20 = I + 2 * msg.msg_init.width,
					*p21 = I + 1 + 2 * msg.msg_init.width,
					*p22 = I + 2 + 2 * msg.msg_init.width,
					*end_pixel = I + 12200;

			// Calculate the row
			int j;
			for (j = 0; p22 != end_pixel; p00++, p01++, p02++,
									 p10++, p11++, p12++,
									 p20++, p21++, p22++,
									 pix_du++, pix_dv++, j++) {
				temp_du = - *p00 - 2 * *p10 - *p20 + *p02 + 2 * *p12 + *p22;
				temp_dv = - *p00 - 2 * *p01 - *p02 + *p20 + 2 * *p21 + *p22;
				*pix_du = saturate(temp_du * 0.25f + 128);
				*pix_dv = saturate(temp_dv * 0.25f + 128);
			}

			// copy to the output line to DMA resource
			e_dma_copy(fb_du_address, (uint8_t *) (BUF_DU_OUT_ADDRESS), (size_t) j);
			e_dma_copy(fb_dv_address, (uint8_t *) (BUF_DV_OUT_ADDRESS), (size_t) j);

			// Move to next lines to calculate
			//fb_du_address += next_line;
			//fb_dv_address += next_line;
			//src_address += next_line;

		//}


		// notify completion
		e_dma_copy(&shared_msg->msg_d2h[core], &msg.msg_d2h[core], sizeof(msg_dev2host_t));

		// Increase frame counter
		frame++;
	}

	return 0;

}

