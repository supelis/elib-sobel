/*
 * c_png.h
 *  Very simple wapper for PNG file read and write
 *
 *  Created on: Apr 14, 2015
 *      Author: rokas
 */

#ifndef C_PNG_H_
	#define C_PNG_H_
#endif /* C_PNG_H_ */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <png.h>
#include <png++/png.hpp>
#include <iostream>
#include <malloc.h>


uint8_t* read_pngpp_file(char *filename, int* width, int* height);
void write_pngpp_file(char* filename, uint8_t* image, int width, int height);
/*void read_png_file(char *filename, uint8_t* image, int* width, int* height);
void write_png_file(char *filename, uint8_t* row_pointers, int width, int height);*/

