/*
 * common.h
 *
 *  Created on: Apr 24, 2015
 *      Author: rokas
 */

#ifndef COMMON_H_
#define COMMON_H_

#define MAXCORES 16
#define ALIGN8 8

#include <inttypes.h>

#define BUF_ADDRESS 0x8f000000
#define BUF_IN_ADDRESS 0x5000
#define BUF_DU_OUT_ADDRESS 0x2000
#define BUF_DV_OUT_ADDRESS (0x2000 + 0x2FA9)

#pragma pack(push, 1)

typedef struct __attribute__((aligned(ALIGN8))) {
	// framebuffer
	uint32_t smem_start;
	uint32_t start_offset;
	uint32_t width;
	uint32_t height;
	uint32_t pixel_bytes;
} msg_init_t;

typedef struct __attribute__((aligned(ALIGN8))) {
	uint32_t value[3];
	uint32_t coreid;
} msg_dev2host_t;

typedef struct __attribute__((aligned(ALIGN8))) {
	uint32_t value[8];
} msg_host2dev_t;

typedef struct __attribute__((aligned(ALIGN8))) {
	msg_init_t msg_init;
	msg_host2dev_t msg_h2d;
	msg_dev2host_t msg_d2h[MAXCORES];
} msg_block_t;

#pragma pack(pop)



#endif /* COMMON_H_ */
