/*
 * c_png.c
 *
 *  Created on: Apr 14, 2015
 *      Author: rokas
 */

#include "c_png.h"

#include <iostream>

uint8_t* read_pngpp_file(char* filename, int *width, int *height) {
	png::image< png::gray_pixel > png(filename);
	*width = png.get_width();
	*height = png.get_height();
	uint8_t* image = (uint8_t*) malloc(*height * *width * sizeof(uint8_t));
	for(int x = 0; x < *width; x++) {
		for(int y = 0; y < *height; y++) {
			image[(x + (y * *width))] = (uint8_t) png.get_pixel(x, y);
		}
	}

	return image;
}

void write_pngpp_file(char* filename, uint8_t* image, int width, int height) {
	png::image< png::gray_pixel > png(width, height);

	for(int y = 0; y < height; y++) {
		for(int x = 0; x < width; x++) {
			png.set_pixel(x, y, image[(x + (y * width))]);
		}
	}

	png.write(filename);
}


/*
void read_png_file(char *filename, uint8_t* image, int* width, int* height) {
	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png) abort();

	png_infop info = png_create_info_struct(png);
	if(!info) abort();

	if(setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

	png_read_info(png, info);

	*width = png_get_image_width(png, info);
	*height = png_get_image_height(png, info);
	color_type = png_get_color_type(png, info);
	bit_depth = png_get_bit_depth(png, info);

	// Read any color_type into 8bit depth, RGBA format.
	// See http://www.libpng.org/pub/png/libpng-manual.txt

	if(bit_depth == 16)
		png_set_strip_16(png);

	if(color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if(png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if(color_type == PNG_COLOR_TYPE_RGB ||
			color_type == PNG_COLOR_TYPE_GRAY ||
			color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if(color_type == PNG_COLOR_TYPE_GRAY ||
			color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_read_update_info(png, info);
	png_bytep *row_pointers;
	size_t total_size = 0, current_size = 0;
	total_size = sizeof(png_bytep*) * (int) height;
	row_pointers = (png_bytep*) malloc(total_size);
	for(int y = 0; y < (int) height; y++) {
		current_size = png_get_rowbytes(png,info);
		row_pointers[y] = (png_byte*) malloc(current_size);
		total_size += current_size;
	}

	png_read_image(png, row_pointers);

	image = (uint8_t*) malloc(total_size);
	memcpy(row_pointers, image, total_size);

	for(int y = 0; y < (int) height; y++) {
		free(row_pointers[y]);
	}

	// Total memory leak FTW
	free(row_pointers);

	fclose(fp);
}

void write_png_file(char *filename, uint8_t* row_pointers, int width, int height) {
	FILE *fp = fopen(filename, "wb");
	if(!fp) abort();

	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) abort();

	png_infop info = png_create_info_struct(png);
	if (!info) abort();

	if (setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

	// Output is 8bit depth, RGBA format.
	png_set_IHDR(
		png,
		info,
		width, height,
		8,
		PNG_COLOR_TYPE_RGBA,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
	);
	png_write_info(png, info);

	// To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
	// Use png_set_filler().
	//png_set_filler(png, 0, PNG_FILLER_AFTER);

	png_write_image(png, row_pointers);
	png_write_end(png, NULL);

	fclose(fp);
}*/



